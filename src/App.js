import {BrowserRouter, Route, Routes} from 'react-router-dom';
import './App.css';
import Layout from './Pages/Components/Layout/Layout';
import HomePage from './Pages/HomePage/HomePage';
import NotFoundPage from './Pages/NotFoundPage/NotFoundPage';
import LoginPage from './Pages/LoginPage/LoginPage';
import DetailPage from './Pages/HomePage/DetailPage/DetailPage';
import RegisterPage from './Pages/HomePage/RegisterPage/RegisterPage';

export default function App() {
  return (
    <div>
      <BrowserRouter>
        <Routes>
          <Route path='*' element={<Layout Component={<NotFoundPage />} />} />
          <Route path='/' element={<Layout Component={<HomePage />} />} />
          <Route path='/login' element={<Layout Component={<LoginPage />} />} />
          <Route
            path='/register'
            element={<Layout Component={<RegisterPage />} />}
          />
          <Route
            path='/detail/:id'
            element={<Layout Component={<DetailPage />} />}
          />
        </Routes>
      </BrowserRouter>
    </div>
  );
}
