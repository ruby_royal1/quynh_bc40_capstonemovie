import {USER_LOGIN} from '../const/userConst';
import {localUserService} from '../../services/localService';

const initialState = {
  userInfo: localUserService.get(),
};

export const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case USER_LOGIN: {
      return {...state, userInfo: action.payload};
    }
    default:
      return state;
  }
};
