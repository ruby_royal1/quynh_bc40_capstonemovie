import moment from 'moment/moment';
import React from 'react';

export default function TabItemMovie({phim}) {
  return (
    <div className='flex p-2 border-b-2'>
      <img src={phim.hinhAnh} alt='_' className='w-40 h-48' />
      <div className='space-x-10'>
        <h2 className='font-extrabold text-lg space-y-5'>{phim.tenPhim}</h2>
        <div className='grid grid-cols-3 gap-5 '>
          {phim.lstLichChieuTheoPhim.slice(0, 9).map((lichChieuTheoPhim) => {
            return (
              <button className='p-2 bg-red-400 text-white w-30 h-12 rounded'>
                {moment(lichChieuTheoPhim.ngayChieuGioChieu).format(
                  'DD/MM/YYYY ~ hh:mm'
                )}
              </button>
            );
          })}
        </div>
      </div>
    </div>
  );
}
