import React, {useEffect, useState} from 'react';
import {movieService} from '../../../services/movieService';
import {Tabs} from 'antd';
import TabItemMovie from './TabItemMovie';

const onChange = (key) => {
  console.log(key);
};

const items = [
  {
    key: '1',
    label: `Tab 1`,
    children: `Content of Tab Pane 1`,
  },
];

export default function TabMovie() {
  let [heThongRap, setHeThongRap] = useState([]);

  useEffect(() => {
    movieService
      .getMovieByTheater()
      .then((res) => {
        setHeThongRap(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  let renderHeThongRap = () => {
    return heThongRap.map((rap) => {
      return {
        key: rap.maHeThongRap,
        label: <img src={rap.logo} alt='_' style={{width: 80}} />,
        children: (
          <Tabs
            tabPosition='left'
            defaultActiveKey='1'
            items={rap.lstCumRap.map((cumRap) => {
              return {
                key: cumRap.maCumRap,
                label: (
                  <button className='font-medium text-lg'>
                    {cumRap.tenCumRap}
                  </button>
                ),
                children: (
                  <div className='h-96 overflow-y-scroll'>
                    {cumRap.danhSachPhim.map((phim) => {
                      return <TabItemMovie phim={phim} />;
                    })}
                  </div>
                ),
              };
            })}
            onChange={onChange}
          />
        ),
      };
    });
  };

  return (
    <div className='container'>
      <Tabs
        // style={{ height: 500 }}
        tabPosition='left'
        defaultActiveKey='1'
        items={renderHeThongRap()}
        onChange={onChange}
      />
    </div>
  );
}
