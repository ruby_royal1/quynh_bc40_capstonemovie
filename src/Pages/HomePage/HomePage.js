import React from 'react';
import ListMovie from './ListMovie/ListMovie';
import TabMovie from './TabMovie/TabMovie';
import TabsPage from './TabsPage/TabsPage';

export default function HomePage() {
  return (
    <div>
      <ListMovie />
      <TabMovie />
      <TabsPage />
    </div>
  );
}
