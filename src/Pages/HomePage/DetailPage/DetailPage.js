import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'
import { movieService } from '../../../services/movieService';
import { Button } from 'antd';
import moment from 'moment';

export default function DetailPage() {

    let { id } = useParams();
    let [movie, setMovie] = useState([])


    useEffect(() => {
        movieService.getMovieDetail(id)
            .then((res) => {
                console.log(res.data.content);
                setMovie(res.data.content);
            })
            .catch((err) => {
                console.log(err);
            });
    }, [])

    return (
        <div className='container'>
            <div className='grid grid-cols-3'>
                <div>
                    <img className='rounded' style={{ width: 400 }} src={movie.hinhAnh} alt="_" />
                </div>
                <div>
                    <h2 className='font-bold'>{movie.tenPhim}</h2>
                    <p>Ngày Khởi Chiếu: {moment(movie.ngayKhoiChieu).format("DD/MM/YYYY")}</p>
                    <p>Giới thiệu phim:
                        <p>{movie.moTa}</p>
                    </p>
                    <Button type='primary' className='bg-red-400'>Đặt Vé Ngay</Button>
                </div>
                <div>
                    <span>Điểm đánh giá: </span>
                    {movie.danhGia}
                </div>
            </div>
        </div>
    )
}
