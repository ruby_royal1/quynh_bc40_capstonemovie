import React from 'react';
import {Card} from 'antd';
import {NavLink} from 'react-router-dom';
const {Meta} = Card;

export default function MovieItem({movie}) {
  return (
    <Card
      className='containerCard px-3 py-3'
      style={{width: '300px'}}
      hoverable
      cover={<img className='h-40' src={movie.hinhAnh} alt='_' />}>
      <Meta
        className='h-32'
        title={movie.tenPhim}
        description={
          <>
            <NavLink to={`/detail/${movie.maPhim}`}>
              <button className='px-5 py-2 bg-red-600 text-white rounded'>
                Xem Ngay
              </button>
            </NavLink>
            <div>{movie.moTa}</div>
          </>
        }
      />
    </Card>
  );
}
