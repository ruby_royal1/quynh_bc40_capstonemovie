import React from 'react';
import './buttonStyles.css';

//sample base application component

function BaseButton(props) {
  return (
    <button
      className='btnPlay'
      onClick={props.onClick}
      value={props.value}></button>
  );
}
export default BaseButton;
