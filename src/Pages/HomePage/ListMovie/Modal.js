import React from 'react';
import './modalStyles.css';

export default function Modal({open, onClose, content}) {
  let autoPlay = '?autoplay=1&mute=1';

  if (!open) return null;
  {
    return (
      <div className='modalOverlay' onClick={onClose}>
        <div className='modal flex items-center justify-center'>
          <iframe
            className='rounded pt-2'
            width={'95%'}
            height={'90%'}
            src={content + autoPlay}
          />
        </div>
      </div>
    );
  }
}
