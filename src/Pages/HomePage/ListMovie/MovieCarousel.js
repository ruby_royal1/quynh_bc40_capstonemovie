import React, {useState, useEffect} from 'react';
import {Carousel} from 'antd';
import {movieService} from '../../../services/movieService';
import Modal from './Modal';
import BaseButton from './Button';

function MovieCarousel() {
  let [movies, setMovies] = useState([]);

  //Modal pop-up video
  const [openModal, setOpenModal] = useState(false);
  const [modalContent, setModalContent] = useState([]);
  function showModal(e) {
    //handle pop-up modal
    setOpenModal(true);
    setModalContent(e.target.value);
    console.log(e.target.value);
  }

  useEffect(() => {
    movieService
      .getMovie()
      .then((res) => {
        setMovies(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  let renderBanner = () => {
    return movies.map((movie) => {
      return (
        <>
          <BaseButton value={movie.trailer} onClick={showModal} />
          <img className='imageBanner' src={movie.hinhAnh} />
        </>
      );
    });
  };

  return (
    <div>
      <Modal
        content={modalContent}
        open={openModal}
        onClose={() => {
          setOpenModal(false);
        }}
      />
      <Carousel
        autoplay
        style={{background: '#000'}}
        effect='fade'>
        {renderBanner()}
      </Carousel>
    </div>
  );
}

export default MovieCarousel;
