import React, {useState} from 'react';
import {movieService} from '../../../services/movieService';
import {useEffect} from 'react';
import MovieItem from './MovieItem';
import MovieCarousel from './MovieCarousel';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import Slider from 'react-slick';

export default function ListMovie() {
  let [movies, setMovies] = useState([]);

  const settings = {
    centerMode: false,
    infinite: false,
    centerPadding: '50px',
    slidesToShow: 3,
    speed: 200,
    rows: 2,
    slidesPerRow: 1,
    dots: true,
    arrows: true,
  };

  useEffect(() => {
    movieService
      .getMovie()
      .then((res) => {
        // console.log(res.data.content);
        setMovies(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  let renderMovies = () => {
    return movies.map((movie) => {
      return (
        <div className='containerCard py-5'>
          <MovieItem movie={movie} key={movie.maPhim} />
        </div>
      );
    });
  };

  return (
    <div>
      <MovieCarousel />
      <Slider
        className='container grid sm:grid-cols-1 md:grid-cols-2 lg:grid-cols-4'
        {...settings}>
        {renderMovies()}
      </Slider>
    </div>
  );
}
