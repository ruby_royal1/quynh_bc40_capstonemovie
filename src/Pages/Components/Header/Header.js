import React from 'react';
import UserMenu from './UserMenu';
import {NavLink} from 'react-router-dom';

export default function Header() {
  return (
    <div className='w-full h-15'>
      <div className='mx-auto flex items-center justify-between'>
        <NavLink to='/'>
          <h1 className='font-extrabold text-4xl text-red-400 animate-bounce hover:animate-bounce hover:text-white'>
            MOVIE FLIX
          </h1>
        </NavLink>
        <div className='grid grid-cols-4 gap-5'>
          <NavLink to='lichchieu'>
            <p className='text-red-500 font-medium hover:text-white hover:duration-300'>
              Lịch Chiếu
            </p>
          </NavLink>
          <NavLink to='tintuc'>
            <p className='text-red-500 font-medium hover:text-white hover:duration-300'>
              Tin Tức
            </p>
          </NavLink>
          <NavLink to='cumrap'>
            <p className='text-red-500 font-medium hover:text-white hover:duration-300'>
              Cụm Rạp
            </p>
          </NavLink>
          <NavLink to='ungdung'>
            <p className='text-red-500 font-medium hover:text-white hover:duration-300'>
              Ứng Dụng
            </p>
          </NavLink>
        </div>
        <div>
          <UserMenu />
        </div>
      </div>
    </div>
  );
}
