import React from 'react'

export default function Footer() {
    return (
        <div className='bg-slate-800 text-white'>
            <div>
                <a href='/'>Questions?Contact us</a>
            </div>
            <div className='flex flex-row justify-around'>
                <ul>
                    <li>FAQ</li>
                    <li>Account</li>
                    <li>Jobs</li>
                    <li>Privacy</li>
                    <li>Contact us</li>
                    <li>Gift Card Terms</li>
                </ul>
                <ul>
                    <li>Help Center</li>
                    <li>Investor Relations</li>
                    <li>Terms of Use</li>
                    <li>Legal Notices</li>
                    <li>Corporate Information</li>
                </ul>
            </div>
        </div>
    )
}
