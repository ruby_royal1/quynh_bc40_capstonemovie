import React from 'react'
import Footer from '../Footer/Footer'
import Header from '../Header/Header'

export default function Layout({ Component }) {
    return (
        <div>
            <div className='mx-auto p-5 bg-gradient-to-r from-indigo-500 flex items-center justify-center'>
                <Header />
            </div>
            <div>
                {Component}
            </div>
            <Footer />
        </div>
    )
}
