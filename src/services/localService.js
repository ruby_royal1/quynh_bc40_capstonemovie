export const localUserService = {
    get: () => {
        let USERINFO_JSON = localStorage.getItem("userInfoJSON");
        return JSON.parse(USERINFO_JSON);
    },
    set: (respond_UserInfo) => {
        localStorage.setItem("userInfoJSON", JSON.stringify(respond_UserInfo))
    },
    remove: () => {
        localStorage.removeItem("userInfoJSON")
    }
}