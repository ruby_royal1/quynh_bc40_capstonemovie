import axios from 'axios';
import {headersConfig, BASE_URL} from './config';

export const movieService = {
  getMovie: () => {
    return axios({
      url: `${BASE_URL}/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP09`,
      method: 'GET',
      headers: headersConfig(),
    });
  },
  getMovieByTheater: () => {
    return axios({
      url: `${BASE_URL}/api/QuanLyRap/LayThongTinLichChieuHeThongRap?maNhom=GP04`,
      method: 'GET',
      headers: headersConfig(),
    });
  },
  getMovieDetail: (movieId) => {
    return axios({
      url: `${BASE_URL}/api/QuanLyPhim/LayThongTinPhim?MaPhim=${movieId}`,
      method: 'GET',
      headers: headersConfig(),
    });
  },
  getMovieBanner: () => {
    return axios({
      url: `${BASE_URL}/api/QuanLyPhim/LayDanhSachBanner`,
      method: 'GET',
      headers: headersConfig(),
    });
  },
};
